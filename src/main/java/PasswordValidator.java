/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author mymac
 */
public class PasswordValidator {
    
    
    
    public static boolean validateLength(String password){
       return password.length()>=8 ? true:false;
    }  
    
    public static boolean validateSpecialChar(String password){
        boolean valid = false;
        for (int i = 0; i< password.length();i++){
            if(password.charAt(i)== '@'||password.charAt(i)== '$'||password.charAt(i)== '+'
                    ||password.charAt(i)== '!'||password.charAt(i)== '#'||password.charAt(i)== '?'||password.charAt(i)== '^'||password.charAt(i)== '&'){
                valid = true;
                break;
             }else{valid = false;}
        } return valid;
        
    }
    
    public static boolean validateUppercase(String password){
       boolean valid = false;
        for(int i = 0; i< password.length();i++){
            //Check every character in the word to see if it contains an uppercase
           if(Character.isUpperCase(password.charAt(i))){
             valid = true;
             break;
            }else {
            valid = false;
            }
        }
        return valid;
    }
   
     public static boolean validateDigit(String password){
         boolean valid = false;
         
         
        for(int i = 0; i< password.length();i++){
            //Check every character in the word to see if it contains a digit
            if(Character.isDigit(password.charAt(i))){
                valid = true;
                break;
            }else{
            valid = false;
            }
        }return valid;
    }
    public static boolean isValid(String password){
        return (validateLength(password) && validateSpecialChar(password) && validateUppercase(password) && validateDigit(password));
    }
}

